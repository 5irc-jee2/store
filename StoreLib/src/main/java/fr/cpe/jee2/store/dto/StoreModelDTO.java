package fr.cpe.jee2.store.dto;

import java.util.HashSet;
import java.util.Set;

public class StoreModelDTO {

	private Integer id;
	private String name;
    private Set<Integer> cardListId = new HashSet<>();

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Set<Integer> getCardListId() {
		return cardListId;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder("StoreModelDTO{");
		stringBuilder.append("id=").append(id);
		stringBuilder.append(", name='").append(name).append('\'');
		stringBuilder.append(", cardListId=").append(cardListId);
		stringBuilder.append('}');
		return stringBuilder.toString();
	}
}
