package fr.cpe.jee2.store.constantes;

public class ConstanteStore {
    public static final String ID_APPLICATION = "STORE_APPLICATION";

    public static final String QUEUE_NAME = "queue_store";
}
