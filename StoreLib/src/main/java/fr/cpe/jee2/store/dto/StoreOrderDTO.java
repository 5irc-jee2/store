package fr.cpe.jee2.store.dto;

public class StoreOrderDTO {
	private int user_id;
	private int card_id;

	public StoreOrderDTO() {
	}

	public int getUser_id() {
		return user_id;
	}

	public int getCard_id() {
		return card_id;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder("StoreOrderDTO{");
		stringBuilder.append("user_id=").append(user_id);
		stringBuilder.append(", card_id=").append(card_id);
		stringBuilder.append('}');
		return stringBuilder.toString();
	}
}
