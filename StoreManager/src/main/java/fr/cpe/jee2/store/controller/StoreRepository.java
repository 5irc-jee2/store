package fr.cpe.jee2.store.controller;

import org.springframework.data.repository.CrudRepository;

import fr.cpe.jee2.store.model.StoreModel;

public interface StoreRepository extends CrudRepository<StoreModel, Integer> {
}
