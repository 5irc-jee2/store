package fr.cpe.jee2.store.model;

import fr.cpe.jee2.card.dto.CardModelDTO;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class StoreModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;

    private transient Set<CardModelDTO> cardList = new HashSet<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<CardModelDTO> getCardList() {
		return cardList;
	}

	public void setCardList(Set<CardModelDTO> cardList) {
		this.cardList = cardList;
	}

	public void addCard(CardModelDTO card) {
		this.cardList.add(card);
	}
	

}
