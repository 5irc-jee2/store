package fr.cpe.jee2.store.controller;

import fr.cpe.jee2.card.constantes.ConstanteCard;
import fr.cpe.jee2.card.dto.CardModelDTO;
import fr.cpe.jee2.dto.EnveloppeDTO;
import fr.cpe.jee2.emitter.controller.BusService;
import fr.cpe.jee2.store.constantes.ConstanteStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cpe.jee2.store.model.StoreModel;

import java.util.Set;

@Service
public class StoreService {

	@Autowired
	StoreRepository storeRepository;

	@Autowired
	private BusService busService;

	private StoreModel store;
	
	public void generateNewStore(String name, int nb) {
		StoreModel store =new StoreModel();
		store.setName(name);
		//On crée l'objet pour envoyer un message au service de carte
		EnveloppeDTO enveloppeDTO = new EnveloppeDTO();
		enveloppeDTO.setMethodName(ConstanteCard.METHOD_RANDOM_CARD);
		enveloppeDTO.addParameters(ConstanteCard.PARAM_RAND_CARD_NB, nb);
		enveloppeDTO.addParameters(ConstanteCard.PARAM_APPLICATION_REPONSE,
				ConstanteStore.ID_APPLICATION);
		busService.sendMsg(enveloppeDTO, ConstanteCard.QUEUE_NAME);
		//List<CardModelDTO> cardList=cardService.getRandCard(nb);
		//TODO generate store
		/*for(CardModelDTO c: cardList) {
			store.addCard(c);
		}
		storeRepository.save(store);
		this.store=store;*/
	}
	
	public boolean buyCard(Integer user_id, Integer card_id) {
		/*Optional<UserModelDTO> u_option=userService.getUser(user_id);
		Optional<CardModelDTO> c_option=cardService.getCard(card_id);
		if(!u_option.isPresent() || !c_option.isPresent()){
			return false;
		}
		UserModelDTO u=u_option.get();
		CardModelDTO c=c_option.get();
		if(u.getAccount() > c.getPrice()) {
			u.addCard(c);
			u.setAccount(u.getAccount()-c.getPrice());
			userService.updateUser(u);
			return true;
		}else {
			return false;
		}*/
		//TODO Ajouter la gestion du buy card
		return true;
	}
	
	public boolean sellCard(Integer user_id, Integer card_id) {
		/*Optional<UserModelDTO> u_option=userService.getUser(user_id);
		Optional<CardModelDTO> c_option=cardService.getCard(card_id);
		if(!u_option.isPresent() || !c_option.isPresent()){
			return false;
		}
		UserModelDTO u=u_option.get();
		CardModelDTO c=c_option.get();

		//c.setStore(this.store);
		c.setUser(null);
		cardService.updateCard(c);
		u.setAccount(u.getAccount()+c.computePrice());
		userService.updateUser(u);*/
		//TODO implementer la method sellCard
		return true;
	}
	
	public Set<CardModelDTO> getAllStoreCard(){
		return this.store.getCardList();
	}
}
